# Emby - Multimedia Streaming server

Your personal streaming server for videos, music and photos. emby automatically converts and streams your media on-the-fly to play on any device.

## Environments

```bash
#!/usr/bin/env
TZ=Europe/Zurich
UID=1001
GID=1001

EMBY_HOSTNAME=emby.example.com

# add video group: `getent group video | cut -d: -f3`
GIDLIST=1001,44

# create volumes first
DATA_VOLUME=entertainment_data

# optional; see docker-compose labels
TRAEFIK_MIDDLEWARES=""
```
